/**
 * View Models used by Spring MVC REST controllers.
 */
package com.limotine.landing.web.rest.vm;
